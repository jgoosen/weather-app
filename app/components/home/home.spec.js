describe('Home component', function() {

	var $compile, 
	    $rootScope, 
	    element, 
	    scope,
	    controller,
	    $q,
	    $state,
	    $timeout,
	    placesService, 
	    weatherApiService,
	    slugifyFilter;

	var weatherResultsTestDataJson = '{"cnt":5,"list":[{"clouds":{"all":75},"coord":{"lat":38.72,"lon":-9.13},"dt":1483525500,"id":2267057,"main":{"humidity":86,"pressure":1016,"temp":8.63,"temp_max":12,"temp_min":5},"name":"Lisbon","sys":{"country":"PT","id":5205,"message":0.1874,"sunrise":1483516489,"sunset":1483550930,"type":1},"visibility":10000,"weather":[{"description":"fog","icon":"50d","id":741,"main":"Fog"}],"wind":{"deg":260,"speed":3.1,"var_beg":210,"var_end":310}},{"clouds":{"all":75},"coord":{"lat":48.85,"lon":2.35},"dt":1483524000,"id":2988507,"main":{"humidity":86,"pressure":1024,"temp":2.99,"temp_max":5,"temp_min":2},"name":"Paris","sys":{"country":"FR","id":5615,"message":0.2105,"sunrise":1483515804,"sunset":1483546101,"type":1},"visibility":10000,"weather":[{"description":"broken clouds","icon":"04d","id":803,"main":"Clouds"}],"wind":{"deg":260,"speed":4.6}},{"clouds":{"all":75},"coord":{"lat":51.51,"lon":-0.09},"dt":1483525200,"id":2643741,"main":{"humidity":81,"pressure":1024,"temp":6.72,"temp_max":8,"temp_min":6},"name":"City of London","sys":{"country":"GB","id":5091,"message":0.1923,"sunrise":1483517105,"sunset":1483545972,"type":1},"visibility":10000,"weather":[{"description":"rain and drizzle","icon":"09d","id":311,"main":"Drizzle"},{"description":"light rain","icon":"10d","id":500,"main":"Rain"}],"wind":{"deg":290,"speed":4.6}},{"clouds":{"all":40},"coord":{"lat":52.43,"lon":4.92},"dt":1483525500,"id":2751980,"main":{"humidity":75,"pressure":1016,"temp":5.25,"temp_max":6,"temp_min":4},"name":"Landsmeer","sys":{"country":"NL","id":5204,"message":0.1935,"sunrise":1483516175,"sunset":1483544497,"type":1},"visibility":10000,"weather":[{"description":"scattered clouds","icon":"03d","id":802,"main":"Clouds"}],"wind":{"deg":320,"speed":10.8,"var_beg":290,"var_end":350}},{"clouds":{"all":75},"coord":{"lat":52.52,"lon":13.4},"dt":1483523400,"id":6545310,"main":{"humidity":80,"pressure":997,"temp":4,"temp_max":4,"temp_min":4},"name":"Berlin Mitte","sys":{"country":"DE","id":4892,"message":0.3093,"sunrise":1483514166,"sunset":1483542431,"type":1},"visibility":10000,"weather":[{"description":"broken clouds","icon":"04d","id":803,"main":"Clouds"}],"wind":{"deg":280,"gust":15.4,"speed":10.3,"var_beg":250,"var_end":310}}]}';
	var weatherResultsTestData = JSON.parse(weatherResultsTestDataJson);
	var placesTestData = {
		lisbon: {
			api: '2267057'
		},
		paris: {
			api: '2988507'
		},
		london: {
			api: '2643741'
		},
		landsmeer: {
			api: '2751980'
		},
		berlin: {
			api: '6545310'
		}
	};

	var placesTestIds = [];
	for (let i in placesTestData) {
		placesTestIds.push(placesTestData[i].api);
	}

	beforeEach(module('weatherApp'));
	beforeEach(module('test.templates')); // This module contains the templates. The name is defined in karma.conf.js

	beforeEach(inject(function(_$compile_, _$rootScope_, _$q_, _$state_, _$timeout_, _placesService_, _weatherApiService_, _slugifyFilter_) {

		$q = _$q_;
		$compile = _$compile_;
		$rootScope = _$rootScope_;
		$state = _$state_;
		placesService = _placesService_;
		weatherApiService = _weatherApiService_;
		$timeout = _$timeout_;
		slugifyFilter = _slugifyFilter_;

		spyOn(placesService, 'get').and.callFake(function() {
			return $q.when(placesTestData);
		});

		spyOn(weatherApiService, 'getCurrentWeatherByIds').and.callFake(function() {
			return $q.when({
				data: {
					list: weatherResultsTestData.list
				}
			});
		});

		scope = $rootScope.$new();
		element = $compile('<home></home>')(scope);
		scope.$digest();

		controller = element.controller('home');

	}));

	it('should render the same number of places grid tiles as there are places in the test data', function() {
		var containerHtmlElement = element[0].querySelectorAll('.wie-container');
		expect(containerHtmlElement.length).toBe(1);

		var numberOfPlacesInTestData = Object.keys(placesTestData).length;
		var numberOfRenderedPlacesGridTiles = element[0].querySelectorAll('.wie-places-grid__place').length;
		expect(numberOfRenderedPlacesGridTiles).toBe(numberOfPlacesInTestData);
	});

	it('should go to the five day weather forecast when clicking a tile.', function() {
		var tileIndex = 1; // click on the second tile.

		var tile = element[0].querySelectorAll('.wie-places-grid__place')[tileIndex];
		tile.click();
		$timeout.flush();

		expect($state.current.component).toEqual('weatherFiveDaysForecastComponent');
		expect($state.params).toEqual(jasmine.objectContaining({
			place: controller.slugify(weatherResultsTestData.list[tileIndex].name), 
			id: weatherResultsTestData.list[tileIndex].id.toString()
		}));

	});

	it('should get the places for showing the weather', function() {
		expect(placesService.get).toHaveBeenCalled();
		expect(weatherApiService.getCurrentWeatherByIds).toHaveBeenCalledWith(placesTestIds);
	});

});