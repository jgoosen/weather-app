(function() {

	'use strict';

	angular.module('weatherApp').component('home', {
		templateUrl: 'components/home/home.html',
		controller: homeCtrl,
		bindings: {}
	});

	homeCtrl.$inject = ['placesService', 'weatherApiService', 'slugifyFilter', '$element'];

	function homeCtrl(placesService, weatherApiService, slugifyFilter, $element) {


		console.log($element);

		var ctrl = this;
		ctrl.places = [];
		ctrl.slugify = slugify;

		this.$onInit = function() {
			getCurrentWeather();
		};

		function getCurrentWeather() {
			getPlaces()
				.then(function(places) {
					var placesIds = [];
					for (var i in places) {
						placesIds.push(places[i].api);
					}
					return getCurrentWeatherByIds(placesIds);
				}, function() {
					alert('Failed to get the places');
				}).then(function(result) {
					ctrl.places = result.data.list;
				}, function() {
					alert('Failed to get the weather forecast');
				});
		}

		function getPlaces() {
			return placesService.get();
		}

		function getCurrentWeatherByIds(placesIds) {
			return weatherApiService.getCurrentWeatherByIds(placesIds);
		}

		function slugify(text) {
			return slugifyFilter(text);
		}

	}
	
})();