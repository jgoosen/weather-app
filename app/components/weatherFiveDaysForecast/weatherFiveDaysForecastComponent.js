(function() {

	'use strict';

	angular.module('weatherApp').component('weatherFiveDaysForecastComponent', {
		templateUrl: 'components/weatherFiveDaysForecast/weatherFiveDaysForecast.html',
		controller: weatherFiveDaysForecastComponentCtrl
	});

	weatherFiveDaysForecastComponentCtrl.$inject = ['$stateParams', 'weatherApiService'];

	function weatherFiveDaysForecastComponentCtrl($stateParams, weatherApiService) {

		var ctrl = this;
		ctrl.forecast = null;
		ctrl.place = {};
		ctrl.place.id = $stateParams.id;


		this.$onInit = function() {
			getForecast().then(function(results) {
				ctrl.place.name = results.data.city.name;
				ctrl.forecast = filterResults(results.data);
			}, function() {
				alert('Failed to get the five day forecast');
			});
		};

		function getForecast() {
			return weatherApiService.getFiveDayWeatherForecastById(ctrl.place.id);
		}

		function filterResults(results) {
			var filteredResult = [];
			results.list.forEach(function(item) {
				var date = new Date((item.dt * 1000));
				if (date.getHours() === 13 && date.getMinutes() === 0) {
					filteredResult.push({
						apiData: item,
						date: {
							day: date.getDate(),
							month: date.getMonth() + 1,
						}
					});
				}
			});
			return filteredResult;
		}
	}
	
})();