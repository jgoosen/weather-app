(function() {

	'use strict';

	angular.module('weatherApp')
		.factory('weatherApiService', weatherApiService);
	
	weatherApiService.$inject = ['$http'];

	function weatherApiService($http) {

		var baseUrl = 'http://api.openweathermap.org/data/2.5/';
		var appid = 'd8f3d489c5e265f0b3261b8c6d65d304';

		var service = {
			getCurrentWeatherByIds: getCurrentWeatherByIds,
			getFiveDayWeatherForecastById: getFiveDayWeatherForecastById
		};

		return service;

		function getCurrentWeatherByIds(ids) {
			var endpoint = baseUrl + 'group?id=' + ids.join() + '&appid=' + appid + '&units=metric';
			return get(endpoint);
		}

		function getFiveDayWeatherForecastById(id) {
			var endpoint = baseUrl + 'forecast?id=' + id + '&appid=' + appid + '&units=metric';
			return get(endpoint);
		}

		function get(endpoint) {
			return $http.get(endpoint, {cache: true});
		}

	}

})();