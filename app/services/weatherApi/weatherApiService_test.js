(function() {

	'use strict';

	describe('places service', function() {

		var weatherApiService,
		    $rootScope,
		    $http,
		    $q,
		    $httpBackend,
		    fakeApiData = {
		    	test: 1,
		    	name: 'sjakie'
		    };

		beforeEach(module('weatherApp'));

		beforeEach(inject(function(_$rootScope_, _weatherApiService_, _$http_, _$q_, _$httpBackend_) {
			weatherApiService = _weatherApiService_;
			$http = _$http_;
			$q = _$q_;
			$httpBackend = _$httpBackend_;
			$rootScope = _$rootScope_;

			$httpBackend.whenGET(/.*/).respond(function () {
		        return [200, fakeApiData, {}];
		    });

		}));

		afterEach(function() {
			$httpBackend.verifyNoOutstandingExpectation();
			$httpBackend.verifyNoOutstandingRequest();
		});

		it('should get current weather by ids', function() {
			var result;
			spyOn($http, 'get').and.callThrough();

			weatherApiService.getCurrentWeatherByIds([1,2]).then(function(data) {
				result = data;
			});

			$httpBackend.flush();

			expect(result.data).toEqual(fakeApiData);
			expect($http.get).toHaveBeenCalled();
			expect($http.get.calls.argsFor(0)).toEqual(['http://api.openweathermap.org/data/2.5/group?id=1,2&appid=d8f3d489c5e265f0b3261b8c6d65d304&units=metric', {cache: true}]);
		});

		it('should get five days weather forecast by id', function() {
			var result;
			spyOn($http, 'get').and.callThrough();

			weatherApiService.getFiveDayWeatherForecastById(20).then(function(data) {
				result = data;
			});

			$httpBackend.flush();

			expect($http.get).toHaveBeenCalled();
			expect($http.get.calls.argsFor(0)).toEqual(['http://api.openweathermap.org/data/2.5/forecast?id=20&appid=d8f3d489c5e265f0b3261b8c6d65d304&units=metric', {cache: true}]);
			expect(result.data).toEqual(fakeApiData);
		});

	});

})();