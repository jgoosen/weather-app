(function() {

	'use strict';

	angular.module('weatherApp')
		.factory('placesService', placesService);
	
	placesService.$inject = ['$q'];

	function placesService($q) {

		var service = {
			get: get
		};

		return service;

		function get() {
			var places = {
				lisbon: {
					api: '2267057'
				},
				paris: {
					api: '2988507'
				},
				london: {
					api: '2643741'
				},
				landsmeer: {
					api: '2751980'
				},
				berlin: {
					api: '6545310'
				}

			};

			return $q.when(places);
		}

	}

})();
