(function() {

	'use strict';

	describe('places service', function() {

		var placesService;
		var $rootScope;

		beforeEach(module('weatherApp'));

		beforeEach(inject(function(_$rootScope_, _placesService_) {
			$rootScope = _$rootScope_;
			placesService = _placesService_;

		}));

		it('should get places', function() {
			var result;

			placesService.get().then(function(data) {
				result = data;
			});

			$rootScope.$apply();

			expect(result).toEqual(jasmine.any(Object));
			expect(result.paris.api).toBe('2988507');
		});

	});

	
})();


