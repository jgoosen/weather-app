(function() {

	'use strict';

	angular.module('weatherApp', [
		'ui.router'
	])
	.config(function($stateProvider, $locationProvider) {

		$locationProvider.html5Mode(true);

		$stateProvider
			.state('home', {
				url: '/',
				component: 'home'
			})
			.state('place', {
				url: '/{place}/{id}',
				component: 'weatherFiveDaysForecastComponent',
			});



	});
	
})();
