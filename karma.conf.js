//jshint strict: false
module.exports = function(config) {
  config.set({

    basePath: './app',

    preprocessors: {
       'components/**/*.html': ['ng-html2js']
     },

    files: [
      'bower_components/angular/angular.js',
      'bower_components/angular-ui-router/release/angular-ui-router.js',
      'bower_components/angular-mocks/angular-mocks.js',
      'app.js',
      'components/**/*.html',
      'services/**/*.js',
      'components/**/*.js',
      'filters//*.js'
    ],

    ngHtml2JsPreprocessor: {
      moduleName: 'test.templates'
    },


    autoWatch: true,

    frameworks: ['jasmine'],

    browsers: ['Chrome'],

    plugins: [
      'karma-ng-html2js-preprocessor',
      'karma-chrome-launcher',
      'karma-firefox-launcher',
      'karma-jasmine',
      'karma-junit-reporter'
    ],

    junitReporter: {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    }

  });
};
