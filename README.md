These steps I have taken when creating this app:
-	I have cloned the Angular seed project from Github and removed the elements that were not necessary for this app. Then I have updated the Angular version to 1.6 in bower.json. Also I have included the ui-router.
-	Started with building the home component and a service for getting the cities. These are now hard coding, but in the future I imagine fetching them from an api. That is why I chose to return a promise instead of a plain javascript object.  Also, I needed to register with Open Weather Map to get an api key.
-	I have created the weatherApiService for fetching all weather data. While working on this, I have written the unit tests.
-	In home.js I have included a flatten function for transforming place names into nice urls. Eventually, I would have created an Angular filter for this.
-	The css has evolved during the creation of the app. The app is responsive. Moreover, I have used flexbox for creating the tiles.
-	Then I started working on the detail component. As mentioned, the place name is reflected in the url. The response of the five-day weather forecast api call is run through a filter to get the forecast for 12 o’clock. Eventually this filter needs to be moved to a service or incorporated in the weatherApiService.

To get this app working, you need to take the following steps:
-	Clone the repository
-	Install the dependencies. Run: $ npm install 
-	Run $ npm start and your browser will open automatically.

To run the unit tests, do: $ npm run test-single-run
